package com.pemkab.pengaduan.impl;

import com.pemkab.pengaduan.model.KategoriPengaduan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class KategoriPengaduanJdbc {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    //List
    public List<KategoriPengaduan> getKategoriPengaduan(){
        String SQL = "select id_kategori_pengaduan as idKategoriPengaduan, " +
                     "nama_kategori_pengaduan as namaKategoriPengaduan  from t_kategori_pengaduan";
        List<KategoriPengaduan> kat = jdbcTemplate.query(SQL, BeanPropertyRowMapper.newInstance(KategoriPengaduan.class));
        return kat;
    }

}
