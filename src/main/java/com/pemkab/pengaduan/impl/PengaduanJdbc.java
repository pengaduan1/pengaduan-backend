package com.pemkab.pengaduan.impl;

import com.pemkab.pengaduan.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

@Repository
public class PengaduanJdbc {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    // List
    public List <Pengaduan> getPengaduan(){
        String SQL = "select a.id_pengaduan as idPengaduan, a.nik as nik, a.nama_lengkap as namaLengkap, " +
                     "a.no_telp as noTelp, a.deskripsi as deskripsi, a.lokasi_aduan as lokasiAduan, a.tanggal as tanggal, " +
                     "a.foto as foto, a.status as status, a.id_kategori_pengaduan as idKategoriPengaduan, " +
                     "k.id_kategori_pengaduan as idKategoriPengaduan, k.nama_kategori_pengaduan as namaKategoriPengaduan, " +
                     "kec.id_kecamatan as idKecamatan, kec.nama_kecamatan as namaKecamatan, " +
                     "dk.id_desa_kelurahan as idDesaKelurahan, dk.nama_desa_kelurahan as namaDesaKelurahan " +
                     "from t_pengaduan a " +
                     "join t_kategori_pengaduan k on a.id_kategori_pengaduan = k.id_kategori_pengaduan " +
                     "join t_kecamatan kec on a.id_kecamatan = kec.id_kecamatan " +
                     "join t_desa_kelurahan dk on a.id_desa_kelurahan = dk.id_desa_kelurahan";
        List<Pengaduan> peng = jdbcTemplate.query(SQL, BeanPropertyRowMapper.newInstance(Pengaduan.class));
        return peng;
    }

    // List Datatables
    public List<Pengaduan> getListPengaduan(DataTablesRequest req){
        String SQL = "select a.id_pengaduan as idPengaduan, a.nik as nik, a.nama_lengkap as namaLengkap, " +
                     "a.no_telp as noTelp, a.deskripsi as deskripsi, a.lokasi_aduan as lokasiAduan, a.tanggal as tanggal, " +
                     "a.foto as foto, a.status as status, a.id_kategori_pengaduan as idKategoriPengaduan, " +
                     "k.id_kategori_pengaduan as idKategoriPengaduan, k.nama_kategori_pengaduan as namaKategoriPengaduan, " +
                     "kec.id_kecamatan as idKecamatan, kec.nama_kecamatan as namaKecamatan, " +
                     "dk.id_desa_kelurahan as idDesaKelurahan, dk.nama_desa_kelurahan as namaDesaKelurahan " +
                     "from t_pengaduan a " +
                     "join t_kategori_pengaduan k on a.id_kategori_pengaduan = k.id_kategori_pengaduan " +
                     "join t_kecamatan kec on a.id_kecamatan = kec.id_kecamatan " +
                     "order by " +(req.getSortCol()+1)+ "  " +req.getSortDir()  +" limit ? offset ? ";
        if(!req.getExtraParam().isEmpty()){
            String nik = (String) req.getExtraParam().get("nik");
            String namaLengkap = (String) req.getExtraParam().get("namaLengkap");
            String noTelp = (String) req.getExtraParam().get("noTelp");
            String idKategoriPengaduan = (String) req.getExtraParam().get("idKategoriPengaduan");
            String idKecamatan = (String) req.getExtraParam().get("idKecamatan");
            String idDesaKelurahan = (String) req.getExtraParam().get("idDesaKelurahan");
            SQL = "select a.id_pengaduan as idPengaduan, a.nik as nik, a.nama_lengkap as namaLengkap, " +
                    "a.no_telp as noTelp, a.deskripsi as deskripsi, a.lokasi_aduan as lokasiAduan, a.tanggal as tanggal, " +
                    "a.foto as foto, a.status as status, a.id_kategori_pengaduan as idKategoriPengaduan, " +
                    "k.id_kategori_pengaduan as idKategoriPengaduan, k.nama_kategori_pengaduan as namaKategoriPengaduan, " +
                    "kec.id_kecamatan as idKecamatan, kec.nama_kecamatan as namaKecamatan, " +
                    "dk.id_desa_kelurahan as idDesaKelurahan, dk.nama_desa_kelurahan as namaDesaKelurahan " +
                    "from t_pengaduan a " +
                    "join t_kategori_pengaduan k on a.id_kategori_pengaduan = k.id_kategori_pengaduan " +
                    "join t_kecamatan kec on a.id_kecamatan = kec.id_kecamatan " +
                    "WHERE nik like concat('%',?,'%') OR nama_lengkap like concat('%',?,'%') " +
                    "OR no_telp like concat('%',?,'%') OR id_kategori like concat('%',?,'%') " +
                    "OR id_kecamatan like concat('%',?,'%') OR id_desa_kelurahan like concat('%',?,'%') " +
                    "order by " +(req.getSortCol()+1)+ "  " +req.getSortDir()  +" limit ? offset ? ";
            return jdbcTemplate.query(SQL, BeanPropertyRowMapper.newInstance(Pengaduan.class), nik, namaLengkap, noTelp, idKategoriPengaduan, idKecamatan, idDesaKelurahan, req.getLength(), req.getStart());
        }else{
            return jdbcTemplate.query(SQL, BeanPropertyRowMapper.newInstance(Pengaduan.class), req.getLength(), req.getStart());
        }
    }

    public Integer getBanyakPengaduan(DataTablesRequest req){
        String SQL = "select count(id_pengaduan) as banyak from t_pengaduan";
        if(!req.getExtraParam().isEmpty()){
            String nik = (String) req.getExtraParam().get("nik");
            String namaLengkap = (String) req.getExtraParam().get("namaLengkap");
            String noTelp = (String) req.getExtraParam().get("noTelp");
            String idKategoriPengaduan = (String) req.getExtraParam().get("idKategoriPengaduan");
            String idKecamatan = (String) req.getExtraParam().get("idKecamatan");
            String idDesaKelurahan = (String) req.getExtraParam().get("idDesaKelurahan");
            SQL =   "select count(id_pengaduan) as banyak from t_pengaduan where " +
                    "nik concat('%',?,'%') or nama_lengkap concat('%',?,'%') or " +
                    "no_telp concat('%',?,'%') or id_kategori_pengaduan concat('%',?,'%') or " +
                    "id_kecamatan concat('%',?,'%') or id_desa_kelurahan concat('%',?,'%')";
            return jdbcTemplate.queryForObject(SQL, Integer.class, nik, namaLengkap, noTelp, idKategoriPengaduan, idKecamatan, idDesaKelurahan);
        }else{
            return this.jdbcTemplate.queryForObject(SQL, null, Integer.class);
        }
    }

    // Insert
    public void insertPengaduan(Pengaduan pengaduan){
        String sql = "insert into t_pengaduan (id_pengaduan,nik,nama_lengkap,no_telp,deskripsi,lokasi_aduan,foto,status,id_kategori_pengaduan,id_kecamatan,id_desa_kelurahan,tanggal) values (?,?,?,?,?,?,?,?,?,?,?,current_timestamp)";
        Object param[] = {pengaduan.getIdPengaduan(),pengaduan.getNik(),pengaduan.getNamaLengkap(),pengaduan.getNoTelp(),
                          pengaduan.getDeskripsi(), pengaduan.getLokasiAduan(),pengaduan.getFoto(),pengaduan.getStatus(),
                          pengaduan.getIdKategoriPengaduan(), pengaduan.getIdKecamatan(), pengaduan.getIdDesaKelurahan(),};
        jdbcTemplate.update(sql, param);
    }

    // By ID
    public Optional<Pengaduan> getPengaduanById(String id){
        String SQL = "select a.id_pengaduan as idPengaduan, a.nik as nik, a.nama_lengkap as namaLengkap, " +
                     "a.no_telp as noTelp, a.deskripsi as deskripsi, a.lokasi_aduan as lokasiAduan, a.tanggal as tanggal, " +
                     "a.foto as foto, a.status as status, a.id_kategori_pengaduan as idKategoriPengaduan, " +
                     "k.id_kategori_pengaduan as idKategoriPengaduan, k.nama_kategori_pengaduan as namaKategoriPengaduan, " +
                     "kec.id_kecamatan as idKecamatan, kec.nama_kecamatan as namaKecamatan, " +
                     "dk.id_desa_kelurahan as idDesaKelurahan, dk.nama_desa_kelurahan as namaDesaKelurahan" +
                     "from t_pengaduan " +
                     "where id_pengaduan = ?";
        Object param[] = {id};
        try{
            return Optional.of(jdbcTemplate.queryForObject(SQL, param, BeanPropertyRowMapper.newInstance(Pengaduan.class)));
        }catch(Exception e){
            return Optional.empty();
        }

    }

    // By ID Kategori Pengaduan
    public List<Pengaduan> getListPengaduanByKategori(String idPengaduan){
        String SQL = "select a.id_pengaduan as idPengaduan, a.nik as nik, a.nama_lengkap as namaLengkap, " +
                     "a.no_telp as noTelp, a.deskripsi as deskripsi, a.lokasi_aduan as lokasiAduan, a.tanggal as tanggal, " +
                     "a.foto as foto, a.status as status, a.id_kategori_pengaduan as idKategoriPengaduan, " +
                     "k.id_kategori_pengaduan as idKategoriPengaduan, k.nama_kategori_pengaduan as namaKategoriPengaduan, " +
                     "kec.id_kecamatan as idKecamatan, kec.nama_kecamatan as namaKecamatan, " +
                     "dk.id_desa_kelurahan as idDesaKelurahan, dk.nama_desa_kelurahan as namaDesaKelurahan" +
                     "from t_pengaduan " +
                     "where id_kategori_pengaduan = ?";

        Object[] param = {idPengaduan};
        return jdbcTemplate.query(SQL,param, (rs,rowNum) -> {
            Pengaduan peng = new Pengaduan();
            peng.setIdPengaduan(rs.getString("idPengaduan"));
            peng.setNik(rs.getString("nik"));
            peng.setNamaLengkap(rs.getString("namaLengkap"));
            peng.setNoTelp(rs.getString("noTelp"));
            peng.setDeskripsi(rs.getString("deskripsi"));
            peng.setLokasiAduan(rs.getString("lokasiAduan"));
            peng.setFoto(rs.getString("foto"));
            peng.setStatus(rs.getString("status"));
            peng.setTanggal(rs.getString("tanggal"));
            KategoriPengaduan kp = new KategoriPengaduan();
            kp.setIdKategoriPengaduan(rs.getString("idKategoriPengaduan"));
            kp.setNamaKategoriPengaduan(rs.getString("namaKategoriPengaduan"));
            Kecamatan kec = new Kecamatan();
            kec.setIdKecamatan(rs.getString("idKecamatan"));
            kec.setNamaKecamatan(rs.getString("namaKecamatan"));
            DesaKelurahan dk = new DesaKelurahan();
            dk.setIdDesaKelurahan(rs.getString("idDesaKelurahan"));
            dk.setNamaDesaKelurahan(rs.getString("namaDesaKelurahan"));
            peng.setKategoriPengaduan(kp);
            peng.setKecamatan(kec);
            peng.setDesaKelurahan(dk);
            return peng;
        });
    }

    // Search Pengaduan
    public List<Pengaduan> getKabupatenSearch(Optional<String> nama){
        String SQL = "select a.id_pengaduan as idPengaduan, a.nik as nik, a.nama_lengkap as namaLengkap, " +
                     "a.no_telp as noTelp, a.deskripsi as deskripsi, a.lokasi_aduan as lokasiAduan, a.tanggal as tanggal, " +
                     "a.foto as foto, a.status as status, a.id_kategori_pengaduan as idKategoriPengaduan, " +
                     "k.id_kategori_pengaduan as idKategoriPengaduan, k.nama_kategori_pengaduan as namaKategoriPengaduan, " +
                     "kec.id_kecamatan as idKecamatan, kec.nama_kecamatan as namaKecamatan, " +
                     "dk.id_desa_kelurahan as idDesaKelurahan, dk.nama_desa_kelurahan as namaDesaKelurahan" +
                     "from t_pengaduan " +
                     "where id_kategori_pengaduan = ? or id_kecamatan = ? or id_kota_kabupaten = ?";

        Object[] param=new Object[1];
        if(nama.isPresent()){
            SQL += " and namaKabupaten like CONCAT('%', ? ,'%')";
            param[0] =nama.get();
        }

        return jdbcTemplate.query(SQL, param, (rs,rowNum) -> {
            Pengaduan peng = new Pengaduan();
            peng.setIdPengaduan(rs.getString("idPengaduan"));
            peng.setNik(rs.getString("nik"));
            peng.setNamaLengkap(rs.getString("namaLengkap"));
            peng.setNoTelp(rs.getString("noTelp"));
            peng.setDeskripsi(rs.getString("deskripsi"));
            peng.setLokasiAduan(rs.getString("lokasiAduan"));
            peng.setFoto(rs.getString("foto"));
            peng.setStatus(rs.getString("status"));
            peng.setTanggal(rs.getString("tanggal"));
            KategoriPengaduan kp = new KategoriPengaduan();
            kp.setIdKategoriPengaduan(rs.getString("idKategoriPengaduan"));
            kp.setNamaKategoriPengaduan(rs.getString("namaKategoriPengaduan"));
            Kecamatan kec = new Kecamatan();
            kec.setIdKecamatan(rs.getString("idKecamatan"));
            kec.setNamaKecamatan(rs.getString("namaKecamatan"));
            DesaKelurahan dk = new DesaKelurahan();
            dk.setIdDesaKelurahan(rs.getString("idDesaKelurahan"));
            dk.setNamaDesaKelurahan(rs.getString("namaDesaKelurahan"));
            peng.setKategoriPengaduan(kp);
            peng.setKecamatan(kec);
            peng.setDesaKelurahan(dk);
            return peng;
        });
    }

    // Ganti Status (Selesai)
    public void gantiStatus(Pengaduan pengaduan){
        String sql = "update t_pengaduan set status = '2' where id_pengaduan = ?";
        Object param[] = {pengaduan.getIdPengaduan()};
        jdbcTemplate.update(sql, param);
    }

    // Ganti Status (Batal)
    public void gantiStatusBatal(Pengaduan pengaduan){
        String sql = "update t_pengaduan set status = '1' where id_pengaduan = ?";
        Object param[] = {pengaduan.getIdPengaduan()};
        jdbcTemplate.update(sql, param);
    }

    // Hapus Pengaduan
    public void deletePengaduan(String id){
        String SQL = "delete from t_pengaduan where id_pengaduan = ?";
        Object param[] = {id};
        jdbcTemplate.update(SQL, param);
    }

}
