package com.pemkab.pengaduan.impl;

import com.pemkab.pengaduan.model.Kecamatan;
import com.pemkab.pengaduan.model.Pengaduan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class KecamatanJdbc {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    // List
    public List<Kecamatan> getKecamatan(){
        String SQL = "select id_kecamatan as idKecamatan, nama_kecamatan as namaKecamatan " +
                     "from t_KEcamatan";
        List<Kecamatan>kec = jdbcTemplate.query(SQL, BeanPropertyRowMapper.newInstance(Kecamatan.class));
        return kec;
    }

}
