package com.pemkab.pengaduan.impl;

import com.pemkab.pengaduan.model.DesaKelurahan;
import com.pemkab.pengaduan.model.Kecamatan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class DesaKelurahanJdbc {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    // List
    public List<DesaKelurahan> getDesaKelurahan(){
        String SQL = "select d.id_desa_kelurahan as idDesaKelurahan, d.nama_desa_kelurahan as namaDesaKelurahan, " +
                     "k.id_kecamatan as idKecamatan, k.nama_kecamatan as namaKecamatan " +
                     "from t_desa_kelurahan d join t_kecamatan k on d.id_kecamatan = k.id_kecamatan";

        return jdbcTemplate.query(SQL, (rs,rowNum) -> {
            DesaKelurahan d = new DesaKelurahan();
            d.setIdDesaKelurahan(rs.getString("idDesaKelurahan"));
            d.setNamaDesaKelurahan(rs.getString("namaDesaKelurahan"));
            Kecamatan k = new Kecamatan();
            k.setIdKecamatan(rs.getString("idKecamatan"));
            k.setNamaKecamatan(rs.getString("namaKecamatan"));
            d.setKecamatan(k);
            return d;
        });
    }

    // List By Kecamatan
    public List<DesaKelurahan> getListDesaKelurahan(String idKec){
        String SQL = "select d.id_desa_kelurahan as idDesaKelurahan, d.nama_desa_kelurahan as namaDesaKelurahan, " +
                     "k.id_kecamatan as idKecamatan, k.nama_kecamatan as namaKecamatan " +
                     "from t_desa_kelurahan d join t_kecamatan k on d.id_kecamatan = k.id_kecamatan " +
                     "where id_kecamatan = ?";

        Object[] param = {idKec};
        return jdbcTemplate.query(SQL,param, (rs,rowNum) -> {
            DesaKelurahan d = new DesaKelurahan();
            d.setIdDesaKelurahan(rs.getString("idDesaKelurahan"));
            d.setNamaDesaKelurahan(rs.getString("namaDesaKelurahan"));
            Kecamatan k = new Kecamatan();
            k.setIdKecamatan(rs.getString("idKecamatan"));
            k.setNamaKecamatan(rs.getString("namaKecamatan"));
            d.setKecamatan(k);
            return d;
        });
//            List<Kabupaten> kab = jdbcTemplate.query(SQL, BeanPropertyRowMapper.newInstance(Kabupaten.class));
        //return prop;
    }
}
