package com.pemkab.pengaduan.ui;

import com.pemkab.pengaduan.impl.DesaKelurahanJdbc;
import com.pemkab.pengaduan.model.DesaKelurahan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class DesaKelurahanAction {

    @Autowired
    private DesaKelurahanJdbc desaKelurahanJdbc;


    // List
    @GetMapping(path= "/api/listdesakelurahanjson")
    public ResponseEntity<List<DesaKelurahan>> listDesaKelurahanJson(){
        return ResponseEntity.ok().body(desaKelurahanJdbc.getDesaKelurahan());
    }

    // List By Kecamatan
    @GetMapping(path= "/listdesakelurahanjson/{id}")
    public ResponseEntity<List<DesaKelurahan>> listDesaKelurahanCari(@PathVariable("id") String id){
        return ResponseEntity.ok().body(desaKelurahanJdbc.getListDesaKelurahan(id));
    }

}
