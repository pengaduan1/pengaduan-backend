package com.pemkab.pengaduan.ui;

import com.pemkab.pengaduan.impl.PengaduanJdbc;
import com.pemkab.pengaduan.model.DataTablesRequest;
import com.pemkab.pengaduan.model.DataTablesResponse;
import com.pemkab.pengaduan.model.Pengaduan;
import com.pemkab.pengaduan.service.PengaduanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Controller
public class PengaduanAction {

    @Autowired
    private PengaduanJdbc pengaduanJdbc;

    @Autowired
    private PengaduanService pengaduanService;

    //List
    @GetMapping(path= "/api/listpengaduanjson")
    public ResponseEntity<List<Pengaduan>> listPengaduanJson(){
        return ResponseEntity.ok().body(pengaduanJdbc.getPengaduan());
    }

    // List Datatables
    @PostMapping(path="/api/listpengaduandatajson")
    public ResponseEntity<DataTablesResponse<Pengaduan>> listPengaduanDataTable(@RequestBody DataTablesRequest dataRequest){
        return ResponseEntity.ok().body(pengaduanService.listPengaduanDataTables(dataRequest));
    }

    //Insert
    @PostMapping("/api/savepengaduanjson")
    public ResponseEntity <Map<String,Object>> savepengaduanjson(@RequestBody Pengaduan pengaduan){
        Map<String, Object> status = new HashMap<>();
        pengaduanJdbc.insertPengaduan(pengaduan);
        status.put("pesan", "Simpan Berhasil");
        return ResponseEntity.ok().body(status);
    }

    // By ID
    @GetMapping(path = "/api/listpengaduanbyidjson/{id}")
    public ResponseEntity<Optional<Pengaduan>> findByID(@PathVariable("id") String id){
        return ResponseEntity.ok().body(pengaduanJdbc.getPengaduanById(id));
    }

    // By ID Kategori
    @GetMapping(path= "/api/listpengaduanbykategorijson/{idx}")
    public ResponseEntity<List<Pengaduan>> listPengaduanByKategori(@PathVariable("idx") String idx){
        return ResponseEntity.ok().body(pengaduanJdbc.getListPengaduanByKategori(idx));
    }

    // Ganti Satus (Selesai)
    @PostMapping("/api/gantistatuspengaduan")
    public ResponseEntity <Map<String,Object>> gantistatuspengaduanjson(@RequestBody Pengaduan pengaduan){
        Map<String, Object> status = new HashMap<>();
        pengaduanJdbc.gantiStatus(pengaduan);
        status.put("pesan", "Selesai");
        return ResponseEntity.ok().body(status);
    }

    // Ganti Status (Batal)
    @PostMapping("/api/gantistatuspengaduanbatal")
    public ResponseEntity <Map<String, Object>> gantistatuspengaduanbataljson(@RequestBody Pengaduan pengaduan){
        Map<String, Object> status = new HashMap<>();
        pengaduanJdbc.gantiStatusBatal(pengaduan);
        status.put("pesan", "Selesai");
        return ResponseEntity.ok().body(status);
    }

    // Hapus Pengaduan
    @DeleteMapping("/api/deletepengaduan/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") String id){
        try {
            pengaduanService.deleteById(id);
            System.out.println("Delete Berhasil");
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (DataAccessException e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

}
