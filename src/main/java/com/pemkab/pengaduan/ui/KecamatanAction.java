package com.pemkab.pengaduan.ui;

import com.pemkab.pengaduan.impl.KecamatanJdbc;
import com.pemkab.pengaduan.model.Kecamatan;
import com.pemkab.pengaduan.model.Pengaduan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class KecamatanAction {

    @Autowired
    private KecamatanJdbc kecamatanJdbc;

    //List
    @GetMapping(path= "/api/listkecamatanjson")
    public ResponseEntity<List<Kecamatan>> listKecamatanJson(){
        return ResponseEntity.ok().body(kecamatanJdbc.getKecamatan());
    }

}
