package com.pemkab.pengaduan.ui;

import com.pemkab.pengaduan.impl.KategoriPengaduanJdbc;
import com.pemkab.pengaduan.model.KategoriPengaduan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class KategoriPengaduanAction {

    @Autowired
    private KategoriPengaduanJdbc kategoripengaduanJdbc;

    //List
    @GetMapping(path= "/api/listkategoripengaduanjson")
    public ResponseEntity<List<KategoriPengaduan>> listKategoriPengaduanJson(){
        return ResponseEntity.ok().body(kategoripengaduanJdbc.getKategoriPengaduan());
    }

}
