package com.pemkab.pengaduan.ui;

import com.pemkab.pengaduan.dto.AkunAdminDto;
import com.pemkab.pengaduan.impl.AkunAdminJdbc;
import com.pemkab.pengaduan.model.AkunAdmin;
import com.pemkab.pengaduan.service.AkunAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class AkunAdminAction {

    @Autowired
    private AkunAdminService akunAdminService;

    @Autowired
    private AkunAdminJdbc akunAdminJdbc;


    @PostMapping("/api/registerakun")
    public ResponseEntity<?> registerakunjson(@RequestBody AkunAdminDto.New akunAdminDto ) throws SQLException {
        Map<String, Object> status = new HashMap<>();
        akunAdminService.saveUser(akunAdminDto);
        status.put("pesan", "Simpan Berhasil");
        return new ResponseEntity<>(akunAdminDto, HttpStatus.CREATED);
    }

    @PostMapping("/api/registeradmin")
    public ResponseEntity <?> getisteradminjson(@RequestBody AkunAdminDto.New akunAdminDto ) throws SQLException{
        Map<String, Object> status = new HashMap<>();
        akunAdminService.saveAdmin(akunAdminDto);
        status.put("pesan", "Simpan Berhasil");
        return new ResponseEntity<>(akunAdminDto,HttpStatus.CREATED);
    }

    @GetMapping(path="/api/listakunjson")
    public ResponseEntity<List<AkunAdmin>> listAkunCari( ){
        return ResponseEntity.ok().body(akunAdminJdbc.getAkun());
    }

    @GetMapping(path="/api/listakunjson/{id}")
    public ResponseEntity<List<AkunAdmin>> listAkunByIdJson(@PathVariable("id") String id ){
        return ResponseEntity.ok().body(akunAdminJdbc.getAkunById(id));
    }

}
