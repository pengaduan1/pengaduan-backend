package com.pemkab.pengaduan.model;

public class Pengaduan {

    private String idPengaduan;
    private String nik;
    private String namaLengkap;
    private String noTelp;
    private String deskripsi;
    private String lokasiAduan;
    private String foto;
    private String status;
    private String tanggal;
    private KategoriPengaduan kategoriPengaduan;
    private Kecamatan kecamatan;
    private DesaKelurahan desaKelurahan;
    private String idKategoriPengaduan;
    private String namaKategoriPengaduan;
    private String idKecamatan;
    private String namaKecamatan;
    private String idDesaKelurahan;
    private String namaDesaKelurahan;

    public String getIdPengaduan() {
        return idPengaduan;
    }

    public void setIdPengaduan(String idPengaduan) {
        this.idPengaduan = idPengaduan;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }

    public String getNoTelp() {
        return noTelp;
    }

    public void setNoTelp(String noTelp) {
        this.noTelp = noTelp;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getLokasiAduan() {
        return lokasiAduan;
    }

    public void setLokasiAduan(String lokasiAduan) {
        this.lokasiAduan = lokasiAduan;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public KategoriPengaduan getKategoriPengaduan() {
        return kategoriPengaduan;
    }

    public void setKategoriPengaduan(KategoriPengaduan kategoriPengaduan) {
        this.kategoriPengaduan = kategoriPengaduan;
    }

    public Kecamatan getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(Kecamatan kecamatan) {
        this.kecamatan = kecamatan;
    }

    public DesaKelurahan getDesaKelurahan() {
        return desaKelurahan;
    }

    public void setDesaKelurahan(DesaKelurahan desaKelurahan) {
        this.desaKelurahan = desaKelurahan;
    }

    public String getIdKategoriPengaduan() {
        return idKategoriPengaduan;
    }

    public void setIdKategoriPengaduan(String idKategoriPengaduan) {
        this.idKategoriPengaduan = idKategoriPengaduan;
    }

    public String getNamaKategoriPengaduan() {
        return namaKategoriPengaduan;
    }

    public void setNamaKategoriPengaduan(String namaKategoriPengaduan) {
        this.namaKategoriPengaduan = namaKategoriPengaduan;
    }

    public String getIdKecamatan() {
        return idKecamatan;
    }

    public void setIdKecamatan(String idKecamatan) {
        this.idKecamatan = idKecamatan;
    }

    public String getNamaKecamatan() {
        return namaKecamatan;
    }

    public void setNamaKecamatan(String namaKecamatan) {
        this.namaKecamatan = namaKecamatan;
    }

    public String getIdDesaKelurahan() {
        return idDesaKelurahan;
    }

    public void setIdDesaKelurahan(String idDesaKelurahan) {
        this.idDesaKelurahan = idDesaKelurahan;
    }

    public String getNamaDesaKelurahan() {
        return namaDesaKelurahan;
    }

    public void setNamaDesaKelurahan(String namaDesaKelurahan) {
        this.namaDesaKelurahan = namaDesaKelurahan;
    }
}
