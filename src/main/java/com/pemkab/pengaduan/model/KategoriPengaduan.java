package com.pemkab.pengaduan.model;

public class KategoriPengaduan {

    private String idKategoriPengaduan;
    private String namaKategoriPengaduan;

    public String getIdKategoriPengaduan() {
        return idKategoriPengaduan;
    }

    public void setIdKategoriPengaduan(String idKategoriPengaduan) {
        this.idKategoriPengaduan = idKategoriPengaduan;
    }

    public String getNamaKategoriPengaduan() {
        return namaKategoriPengaduan;
    }

    public void setNamaKategoriPengaduan(String namaKategoriPengaduan) {
        this.namaKategoriPengaduan = namaKategoriPengaduan;
    }
}
