package com.pemkab.pengaduan.model;

public class DesaKelurahan {

    private String idDesaKelurahan;
    private String namaDesaKelurahan;
    private DesaKelurahan desaKelurahan;
    private Kecamatan kecamatan;

    public String getIdDesaKelurahan() {
        return idDesaKelurahan;
    }

    public void setIdDesaKelurahan(String idDesaKelurahan) {
        this.idDesaKelurahan = idDesaKelurahan;
    }

    public String getNamaDesaKelurahan() {
        return namaDesaKelurahan;
    }

    public void setNamaDesaKelurahan(String namaDesaKelurahan) {
        this.namaDesaKelurahan = namaDesaKelurahan;
    }

    public DesaKelurahan getDesaKelurahan() {
        return desaKelurahan;
    }

    public void setDesaKelurahan(DesaKelurahan desaKelurahan) {
        this.desaKelurahan = desaKelurahan;
    }

    public Kecamatan getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(Kecamatan kecamatan) {
        this.kecamatan = kecamatan;
    }
}
