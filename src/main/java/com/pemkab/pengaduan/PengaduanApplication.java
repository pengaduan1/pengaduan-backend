package com.pemkab.pengaduan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.pemkab.pengaduan.ui","com.pemkab.pengaduan.impl","com.pemkab.pengaduan.service","com.pemkab.pengaduan.config"})
public class PengaduanApplication {

	public static void main(String[] args) {
		SpringApplication.run(PengaduanApplication.class, args);
	}

}
