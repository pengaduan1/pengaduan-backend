package com.pemkab.pengaduan.service;

import com.pemkab.pengaduan.impl.PengaduanJdbc;
import com.pemkab.pengaduan.model.DataTablesRequest;
import com.pemkab.pengaduan.model.DataTablesResponse;
import com.pemkab.pengaduan.model.Pengaduan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

@Service
public class PengaduanService {

    @Autowired
    private PengaduanJdbc pengaduanJdbc;

    // Hapus Pengaduan
    public void deleteById(String id) throws DataAccessException{
        pengaduanJdbc.deletePengaduan(id);
    }

    // List DataTables
    public DataTablesResponse<Pengaduan> listPengaduanDataTables (DataTablesRequest req) {
        DataTablesResponse dataTableResponse = new DataTablesResponse();
        dataTableResponse.setData(pengaduanJdbc.getListPengaduan(req));
        Integer total = pengaduanJdbc.getBanyakPengaduan(req);
        dataTableResponse.setRecordsFiltered(total);
        dataTableResponse.setRecordsTotal(total);
        dataTableResponse.setDraw(req.getDraw());
        return dataTableResponse;
    }

}
