package com.pemkab.pengaduan.service;

import com.pemkab.pengaduan.dto.GroupUserDto;
import com.pemkab.pengaduan.impl.RolesJdbc;
import com.pemkab.pengaduan.model.GroupUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroupUserService {

    @Autowired
    private RolesJdbc rolesJdbc;

    public List<GroupUser> findAll() throws EmptyResultDataAccessException {
        return rolesJdbc.getGroupUser();
    }

    public void deleteById(GroupUserDto.Information value) throws DataAccessException {
        rolesJdbc.deleteById(value);
    }

}
