package com.pemkab.pengaduan.service;

import com.pemkab.pengaduan.dto.AkunAdminDto;
import com.pemkab.pengaduan.impl.AkunAdminJdbc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class AkunAdminService {

    @Autowired
    private AkunAdminJdbc akunAdminJdbc;

    public void saveAdmin(AkunAdminDto.New akunAdminDto) throws SQLException {
        akunAdminDto.setId(UUID.randomUUID().toString());
        Map<String, Object> paramRegAcc = new HashMap<>();
        paramRegAcc.put("id", UUID.randomUUID().toString());
        paramRegAcc.put("idUser", akunAdminDto.getId());
        paramRegAcc.put("idGroup", 2);
        akunAdminJdbc.insertGroupUser(paramRegAcc);
        akunAdminJdbc.registerAkun(akunAdminDto);
    }

    public void saveUser(AkunAdminDto.New akunAdminDto) throws SQLException {
        akunAdminDto.setId(UUID.randomUUID().toString());
        Map<String, Object> paramRegAcc = new HashMap<>();
        paramRegAcc.put("id", UUID.randomUUID().toString());
        paramRegAcc.put("idUser", akunAdminDto.getId());
        paramRegAcc.put("idGroup", 3);
        akunAdminJdbc.insertGroupUser(paramRegAcc);
        akunAdminJdbc.registerAkun(akunAdminDto);
    }

}
